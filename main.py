from pathlib import Path, PurePath
import os
import subprocess
import argparse
import shutil

base_directory = Path(Path.home(), ".crostini-apps")
iconsPaths = [Path("/usr/share/pixmaps")]
iconStringStart = 'Icon='
nameStringStart = 'Name='
execStringStart = 'Exec='
newIconBasePath = base_directory.joinpath('icons/hicolor/48x48/apps')
newAppBasePath = base_directory.joinpath('applications')


def convertIcon(input, output):
    print(f"Converting {input} to {output}")
    parent = Path(output.parent)
    if not parent.exists():
        parent.mkdir(parents=True)

    subprocess.run([
        "inkscape", "-w", "1024", "-h", "1024", input, "--export-filename",
        output
    ])


def main():
    parser = argparse.ArgumentParser(
        description=
        'Helper Tool to adjust installed applications desktop entries to work better under Chrome OS'
    )
    parser.add_argument('--icon',
                        '-i',
                        action='store_true',
                        help='Attempts to fix broken App Icons')
    parser.add_argument(
        '--clean',
        '-c',
        action='store_true',
        help=f'Delete previously created configurations ({base_directory})')
    parser.add_argument(
        '--convert',
        nargs='*',
        help='List of App Names to manually specify scaling for')
    parser.add_argument('--scale',
                        default=0.5,
                        help='Scale parameter for sommelier')
    parser.add_argument('--dpi',
                        default=160,
                        help='Dpi parameter for sommelier')

    args = parser.parse_args()
    # print(args)

    if args.clean:
        # base_directory.rmdir()
        print(f"Cleaning up {base_directory}")
        shutil.rmtree(base_directory, ignore_errors=True)

    if not args.icon and (args.convert is None or len(args.convert) == 0):
        # Only output the error if no other action has been complete
        if not args.clean:
            print("At least one of --icon or --convert must be specified")
        return

    if not base_directory.exists():
        base_directory.mkdir()

    # print(base_directory)

    dataPaths = list(
        filter(lambda e: base_directory != e,
               map(lambda e: Path(e), os.environ['XDG_DATA_DIRS'].split(":"))))

    applications = []
    # Gather all Applications
    for path in dataPaths:
        # print(path)
        applications.extend(list(path.glob("applications/*.desktop")))

    numConvertedIcons = 0
    convertedApps = []
    for app in applications:
        icons = None
        lines = None
        shouldWriteNewEntry = False
        with app.open() as f:
            lines = [line for line in f]

        appName = next((line.rstrip()[len(nameStringStart):]
                        for line in lines if line.startswith(nameStringStart)),
                       None)

        print(f"Processing: {appName} : {app}")

        if args.convert is not None:

            # print(appName)
            if appName in args.convert:
                print(f"Attempting to apply sommelier patch to {appName}")

                def processExecLine(line: str):
                    if line.startswith(execStringStart):
                        return f"{execStringStart}sommelier -X --scale={args.scale} --dpi={args.dpi} {line[len(execStringStart):]}"

                    return line

                lines = list(map(processExecLine, lines))

                shouldWriteNewEntry = True

        if args.icon:
            iconName = next(
                (PurePath(line.rstrip()[len(iconStringStart):])
                 for line in lines if line.startswith(iconStringStart)), None)

            if iconName is None:
                print("Found no icon")
                continue
            print(f"Found icon: {iconName}")
            gotPngIcon = False
            if iconName.is_absolute():
                if iconName.suffix == '.png':
                    print("Found absolute path to png image, skipping...")
                    continue
                # sheet
                if len(iconName.suffix) == 0:
                    print(
                        "Absolute path does not specify extension so we can just convert"
                    )
                    # Means we got no extension for this so we can easily fix it
                    svgPath = Path(iconName.with_suffix('.svg'))
                    if svgPath.exists():
                        numConvertedIcons += 1
                        convertIcon(svgPath, iconName.with_suffix('.png'))
                else:
                    print(
                        "Absolute path specifies extension so we need to create a new .desktop file"
                    )
                    newIconName = iconName.stem

                    def processIconLine(line: str):
                        if line.startswith(iconStringStart):
                            return f"{iconStringStart}{newIconName}\n"

                        return line

                    lines = list(map(processIconLine, lines))

                    shouldWriteNewEntry = True

                    output = newIconBasePath.joinpath(f"{newIconName}.png")
                    convertIcon(iconName, output)
                    # return

            else:
                print(f"Performing a search for: {iconName}")
                availableIcons = []
                for iconPath in iconsPaths:
                    glob = f"{iconName}.*"
                    iconSearch = list(iconPath.glob(glob))
                    firstPng = next(
                        (i for i in iconSearch if i.suffix == '.png'), None)
                    if firstPng != None:
                        print(f"Found PNG: {firstPng} ignoring...")
                        gotPngIcon = True
                        break
                    else:
                        availableIcons.extend(iconSearch)

                if not gotPngIcon:
                    # print("doing more expansive search")
                    for path in dataPaths:
                        path = path.joinpath("icons/hicolor/")
                        # print(path)
                        glob = f"**/apps/{iconName}.*"
                        # /usr/share/icons/hicolor/48x48/apps/org.inkscape.Inkscape.png
                        # print(glob)
                        iconSearch = list(path.glob(glob))
                        # print(iconSearch)

                        firstPng = next(
                            (i for i in iconSearch if i.suffix == '.png'),
                            None)
                        if firstPng != None:
                            print(f"Found PNG: {firstPng} ignoring...")
                            gotPngIcon = True
                            break
                        else:
                            availableIcons.extend(iconSearch)

                    if not gotPngIcon:
                        firstSvg = next(
                            (i for i in availableIcons if i.suffix == '.svg'),
                            None)
                        print(f"First SVG: {firstSvg}")
                        if firstSvg != None:
                            output = newIconBasePath.joinpath(
                                f"{iconName}.png")
                            # print(f"Converting svg icon to: {output}")
                            numConvertedIcons += 1
                            convertIcon(firstSvg, output)

        if shouldWriteNewEntry:
            newDesktopFile = Path(newAppBasePath.joinpath(app.name))
            if not newDesktopFile.parent.exists():
                newDesktopFile.parent.mkdir(parents=True)
            print(f"Writing new desktop file to: {newDesktopFile}")
            with newDesktopFile.open('w') as f:
                f.writelines(list(lines))

            convertedApps.append(appName)

    # print(applications)
    unConvertedApps = [
        app for app in args.convert or [] if app not in convertedApps
    ]
    convertedAppsStr = ""
    if args.convert is not None:
        if len(unConvertedApps) == 0:
            convertedAppsStr = "Successfully applied manual scaling patch to all specified"
        else:
            convertedAppsStr = f"Failed to apply manual scaling patch to following: {unConvertedApps}"
    print(f"Finished! Converted {numConvertedIcons} icons! {convertedAppsStr}")
    pass


if __name__ == "__main__":
    main()