# Crostini Application Corrector

## Motivation

As amazing as Crostini is it does have a couple of issues here and there. Firstly, a lack of support for SVG icons, which causes a number of applications to display the default icon rather than their own. Secondly, is lack of proper display scaling for X apps. These both annoyed me a bit so I decided to write this small script to help deal with these problems.

## Features

- Correct broken SVG app icons
- Apply custom scaling to particular apps
- All changes are non-destructive and are applied into an 'override' folder to be easily undoable

## Dependencies

- inkscape
- python3

e.g.

```bash
sudo apt install inkscape python3
```

## Usage

Likely the easiest way to use this is to download `main.py` from here or clone the repository.
i.e:

```bash
git clone https://gitlab.com/_syte_/crostini-application-corrector.git
```

Then enter the directory containing `main.py`:
e.g.

```bash
cd crostini-application-corrector
```

**Important** For the overriding to work you need to run this:

```
systemd edit --user cros-garcon
```

and paste in this text

```
[Service]
Environment="XDG_DATA_DIRS=%h/.crostini-apps:%h/.local/share:%h/.local/share/flatpak/exports/share:/var/lib/flatpak/exports/share:/usr/local/share:/usr/share"
```

> For those interested. This is required to ensure the `garcon` tool, which is what provides application information to ChromeOS will search our custom override directory for icons. Currently I was only able to find out how to set the variable, not just add our path to it. So if anyone knows a better way then please let me know! This setup is grand as long as no more paths are added to this variable

### Commands

Check the help for possible commands, i.e.:

```bash
python3 main.py --help
```

#### Fix Icons

To correct SVG icons for applications that don't appear properly in ChromeOS run:

```bash
python3 main.py --icon
```

and reboot your device.

#### App Scaling

To apply custom scaling for an app run:

```bash
python3 main.py --convert APP_NAME --scale SCALE --dpi DPI
```

- `APP_NAME` Should be the name exactly how it appears in your ChromeOS app list
- `SCALE` & `DPI` should be the scale & dpi properties you want passed to be passed to sommelier for scaling

#### Clean/Undo

To undo all changes applied:

```bash
python3 main.py --clean
```

You may also want to undo the **Important** step from above (although it shouldn't intefere with anything) by:

```bash
systemctl edit --user cros-garcon
```

and removing the contents of the file

> I feel like there's a better solution than this
